import socket

def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(('', 9091))
    clients = []
    print('start server')
    while True:
        data, addr = sock.recvfrom(1024)
        print('connected:', addr)
        print(data)
        if addr not in clients:
            clients.append(addr)
        for client in clients:
            if client == addr:
                mess = input()
                sock.sendto(mess.encode('utf-8'),client)
                continue
            
            sock.sendto(data, client)
    sock.close()


######SERVER##############
if __name__ == '__main__':
    main()
